/*
**	Functional handler for when the search button is clicked
**	It fetches the entered name from the text box (Defaults to Bill Murry if it's empty)
**	And sends an ajax request to get the data.  After receiving the data it lists out 
**	Chronologically all the movies that the specified name acted in
*/
function searchClicked() {
    var name = document.getElementById('nameInput').value, 
    	parameters = {name: name.length > 0 ? name : 'Bill Murray'},
    	html;
    

    $.get( '/api/search', parameters, function(data) {
        if (data.error) {
            html = "Name search error!"
        }
        else {
            html = 'Movies Chronologically that ' + data.actors.results[0].name + ' acted in<br /><br />';

            for (var i = 0; i < data.movies.length; i++) {
                html += data.movies[i].title + ',  ' + data.movies[i].release_date + '<br />';
            }
        }

        document.getElementById('displayList').innerHTML = html;
    });
}