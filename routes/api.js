/*
 * Serve JSON to our AngularJS client
 */

/*
**	Node api handler for my search request
**	After receiving a search request we connect to themoviedb, search for a person by
**	The name provided, then, assuming an actor is found matching that name (if multiple are found
**	We assume that the first is the most likely match) we search for all movie credits using that
**	Actor's id
**	@param req the request parameters passed in, in this case the name to search for
**	@param res the response json to return after the query is finished
*/
exports.search = function (req, res) {
	var MovieDB = require('moviedb')('ac4e97dea121296914cc69cbb3d21597'),
		id, sortedArray;

	MovieDB.searchPerson({query: req.query.name}, function(err, a_res) {
		if (err || !a_res || a_res.results.length === 0) {
			res.json({error: err ? err : 'No Name Results Returned'});
		}
		else {
			id = a_res.results[0].id;

			MovieDB.personCredits({id: id}, function(error, m_res) {
				sortedArray = m_res.cast.sort(dateSort);
				res.json({movies: sortedArray, actors: a_res});
			});
		}
	});
}

/*
**	Chronological sorting function
**	I had trouble including the sorting in the query request to themoviedb so I simply sorted the 
**	Array of movies found here before passing them back to the UI
*/
function dateSort(a, b) {
	if (a.release_date > b.release_date) {
		return -1;
	}
	else {
		return 1;
	}
}